#!/bin/bash

echo "*** Starting bmcloud installer"
version=$(curl -s https://gitlab.com/hidupv3/helpers/bmcloud/release/raw/master//.version)

echo "Installing version ${version} in /usr/local/bin/bmcloud"

curl -s https://gitlab.com/hidupv3/helpers/bmcloud/release/raw/master/bmcloud > /tmp/bmcloud-${version}
sudo mv /tmp/bmcloud-${version} /usr/local/bin/bmcloud-${version}
sudo chown root.root /usr/local/bin/bmcloud-${version}
sudo chmod 755 /usr/local/bin/bmcloud-${version}
sudo ln -sf /usr/local/bin/bmcloud-${version} /usr/local/bin/bmcloud
sudo ln -sf /usr/local/bin/bmcloud-${version} /usr/local/bin/bmcloud-cli
sudo ln -sf /usr/local/bin/bmcloud-${version} /usr/local/bin/bmcloud-server

echo "Instalation finished..."
